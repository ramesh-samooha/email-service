### Simple Email Service

Simple email service send email with specified content and attachments.

The application working directory as "./bin/config/apps.properties" files helps to configure emails.

```

mail.service.by=Samooha
mail.logs.dir=/Users/ramesh/Samooha/sftp-email/target/matrix-star/logs
mail.content.file=/Users/ramesh/Samooha/sftp-email/target/matrix-star/bin/email-content.txt
mail.attachments.dir=/Users/ramesh/Samooha/sftp-email/target/matrix-star/outbox
mail.attachments-archive.dir=/Users/ramesh/Samooha/sftp-email/target/matrix-star/error
mail.attachments.delete=false
mail.subject=Samooha PO Import Error Files
mail.from=matrix-star@samooha.biz
mail.to=sample@gmail.com,sample@live.com
mail.cc=rameshselvaraj@samooha.com

```

The application working directory as "./bin/config/email.properties" files helps to configure smtp.

```

mail.smtp.writetimeout=60000
mail.smtp.from=simple@gmail.biz
mail.smtp.connectiontimeout=60000
mail.smtp.host=mail.simple.biz
mail.smtp.timeout=600000
mail.smtp.starttls.enable=true
mail.smtp.port=587
mail.smtp.auth=true
mail.smtp.ssl.trust=*
mail.smtp.user=matrix-star@gmail.biz
mail.smtp.password=my-password
mail.transport.protocol=smtp

```

Application allows to modify and reload the configurations of (app.properties) by using the following url via the browser.

[http://localhost:7071/admin.html](http://localhost:7071/admin.html)

The log files are created under the application sub folder of "./logs".
Monthly once create sub folder for each month logs. The following url to visit the logs by browser.

[http://localhost:7071](http://localhost:7071)

#### Technical Specification

- Application run by Java 11.
- Email sends via sftp protocol.
- SpringBoot framework used to run service and web support. 
- Application use 7071 port for web service, In case to change the port can be modified in to the "application.properties" file.
- Web client boostrap library used. 
